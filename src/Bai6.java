public class Bai6 {
    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        double kq = (double) a/b; // int >> double ép kiểu rộng từ kiểu nhỏ sang kiểu lớn
        System.out.println(kq);

        // ép kiểu hẹp
        int c = 128;
        byte d = (byte) c;
        System.out.println(d);

        int e = 15;
        byte f = (byte) e; // ép kiểu từ int sang byte
        System.out.println(e);
        System.out.println(f);
    }
}
