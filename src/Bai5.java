public class Bai5 {
    public static void main(String[] args) {
        // khai báo biến
        int a;
        byte tuoi;
        float diemToan;
        // Khai báo nhiều biến 1 lúc cùng kiểu
        int c,d,e,f;

        //khởi tạo biến
        int tuoiCon = 10;
        float diemVan = 7.5f; // với kiểu float phải thêm f
        double diemAnh = 7.5;

        System.out.println(tuoiCon);

        tuoiCon = 15;
        System.out.println(tuoiCon);

        // Hằng số
        final int DOISO = 100;
    }
}
