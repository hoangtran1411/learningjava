import java.util.Scanner;

public class Bai11_baitap {
    public static void main(String[] args) {
        // tim x, y khi biet tong va hieu cua chung
        Scanner sc = new Scanner(System.in);
        System.out.println("Mời bạn nhập tổng 2 số: ");
        int tong = sc.nextInt();
        System.out.println("Mời bạn nhập hiệu 2 số: ");
        int hieu = sc.nextInt();
        double x = (tong + hieu) / 2;
        double y = (tong - hieu) / 2;

        System.out.println("So x la: " + x);
        System.out.println("So y la: " + y);

    }
}
