public class Bai9 {
    public static void main(String[] args) {

        int i = 15;
        // kiểm tra i > 0 và i < 10 không
        System.out.println(i>0 && i<10 );

        // kiểm tra xem i > 10 hoặc i < -1
        System.out.println(i>10 || i<-1);

        // phép phủ định --> thêm ! vào trước phép so sánh
        System.out.println(!(i>10 || i<-1));

        // Toán tử tiền tố hậu tố
        int x = 100;
        int y = 90;
        int z = 80;
        int t = 70;

        x++; // x = x + 1
        ++y; // tăng giá trị y lên 1
        z--; // giảm giá trị z xuống 1
        --t; // giảm gía trị t xuống 1

        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(t);
    }
}
