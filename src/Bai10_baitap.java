import java.util.Scanner;

public class Bai10_baitap {
    public static void main(String[] args) {
        // Tính chu vi, diện tích hình tròn

        Scanner sc = new Scanner(System.in);

        System.out.println("Mời nhập bán kính r: ");
        double r = sc.nextDouble();
        double cv = 2 * Math.PI * r;
        double dt = Math.PI * Math.pow(r,2);

        // Xuất kết quả
        System.out.println("Chu vi là : " + cv);
        System.out.println("Dien tich là : " + dt);
    }
}
