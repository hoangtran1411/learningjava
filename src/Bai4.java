public class Bai4 {
    public static void main(String[] args) {
        //Kiem tra kieu so ngyen
        System.out.println("max cua kieu int => " + Integer.MAX_VALUE);
        System.out.println("min cua kieu int => " + Integer.MIN_VALUE);

        //Kiem tra kieu so byte
        System.out.println("max cua kieu byte => " + Byte.MAX_VALUE);
        System.out.println("min cua kieu byte => " + Byte.MIN_VALUE);
    }
}
