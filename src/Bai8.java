public class Bai8 {
    public static void main(String[] args) {
        // gán cộng
        int x = 8;
        x+= 5;
        System.out.println("x += "+ x);

        // gán trừ
        int x2 = 8;
        x2-= 5;
        System.out.println("x2 -= "+ x2);

        // gán nhân
        int x3 = 8;
        x3*= 5;
        System.out.println("x3 *= "+ x3);

        //các phép so sánh

        int a = 7;
        int b = 9;
        System.out.println("a==b => "+(a==b));
        System.out.println("a>=b => "+(a>=b));
        System.out.println("a<=b => "+(a<=b));
        System.out.println("a!=b => "+(a!=b));
    }
}
