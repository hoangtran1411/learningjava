import java.util.Scanner;

public class Bai10 {
    public static void main(String[] args) {
     /*   Scanner sc = new Scanner(System.in); // khoi tao doi tuong sc

        // cho phep nhap chuoi
        System.out.println("Moi nhap vao mat khau cap 1: ");
        String mk1 = sc.nextLine();
        System.out.println("Mat khau cap 1 la "+ mk1);

        // nhap so nguyen
        System.out.println("Moi nhap vao 1 so nguyen: ");
        int a = sc.nextInt();
        System.out.println("So nguyen vua nhap la: "+ a);

        // nhap so float
        System.out.println("Moi nhap vao 1 so thuc x: ");
        float x = sc.nextFloat();
        System.out.println("x = " + x);

        // cho phep nhap chuoi
        System.out.println("Moi nhap vao mat khau cap 2: ");
        String mk2 = sc.nextLine();
        System.out.println("Mat khau cap 2 la "+ mk2);*/

        // Cach 2 tao doi tuong Scan moi lan nhap
        System.out.println("Moi cu nhap ten: ");
        String ten = new Scanner(System.in).nextLine(); // tao moi doi tuong scanner
        System.out.println("Ten cua cu la: " + ten);

        // nhap chuoi
        System.out.println("Moi cu nhap gioi tinh: ");
        String gt = new Scanner(System.in).nextLine(); // tao moi doi tuong scanner
        System.out.println("gioi tinh cua cu la: " + gt);

        // nhap float
        System.out.println("Moi cu nhap chieu cao: ");
        float cao = new Scanner(System.in).nextFloat(); // tao moi doi tuong scanner
        System.out.println("Chieu cao cua cu la: " + cao);

    }
}
