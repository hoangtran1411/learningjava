import java.util.Scanner;

public class Bai11_If_else {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Mời bạn nhập điểm trung bình: ");
        double dtb = sc.nextDouble();
        if (dtb >= 8 && dtb <= 10)
            System.out.println("Bạn xl giỏi");
        else if (dtb >= 7 && dtb < 8)
            System.out.println("Bạn xl khá");
        else if (dtb >= 5 && dtb < 7)
            System.out.println("Bạn xl tb");
        else
            System.out.println("Bạn tạch");
    }
}
