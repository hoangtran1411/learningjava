import java.util.Scanner;

public class Bai10_BaiTap_HinhChuNhat {
    public static void main(String[] args) {
        // Tính chu vi, diện tích hình chữ nhật
        //1. nhập vào 2 số thực dương a,b từ bàn phím a và b là chiều dài và chiều rộng của hcn
        Scanner sc = new Scanner(System.in);
        System.out.println("Mời nhập chiều dài a: ");
        double a = sc.nextDouble();
        System.out.println("Mời nhập chiều rộng b: ");
        double b = sc.nextDouble();

        //2. in ra màn hình chu vi và diện tích hcn
        double s = a * b;
        double p = (a + b) * 2;
        System.out.println("Diện tích s: " + s);
        System.out.println("Chu vi p: " + p);
    }

}
